# RSAP-Go

This is a dev repo of the RedShift Application Protocol. While the project is
in a working and usable state, it is to be considered unstable. Use at your own risk.

This is an application messaging protocol developed for use in electron applications.
It is inspired by Microsoft's Language Server Protocol and uses JSON-RPC style
messaging to allow an electron client UI to communicate with an underlying service
over STD in, out, and err. There is a Javascript client available at [https://gitlab.com/redshift-development/rsap-js](https://gitlab.com/redshift-development/rsap-js)

### Usage

```go
package main

import (
	"context"
	"encoding/json"
	"log"
	"os"

	"gitlab.com/redshift-development/rsap-go/pipe"
	"gitlab.com/redshift-development/rsap-go/rsmp"
)

func main() {
	router := rsmp.NewRouter()
	router.Route("test.echo", echoHandler)
	stdio := pipe.NewStdPipe(os.Stdin, os.Stdout, os.Stderr)
	// Or you can use a named pipe (Windows)
	// stdio, err := pipe.NewNamedPipe("test_service")
	// if err != nil {
	// 	log.Fatal("failed to started named pipe: " + err.Error())
	// }
	srv := rsmp.NewService(router, stdio)
	srv.Listen() // non-blocking
}

func echoHandler(ctx context.Context, message json.RawMessage) error {
	req := EventMessage{}
	err := rsmp.BindData(message, &req)
	if err != nil {
		log.Println("json decode error:", err.Error())
		return rsmp.RespondError(ctx)(
			&rsmp.ErrorMessage{
				Code:    rsmp.ErrorBadData,
				Message: "Invalid echo test request!",
				Data:    err.Error(),
			},
		)
	}
	return rsmp.Respond(ctx)(req)
}

type EventMessage struct {
	SomeMessage string `json:"someMessage"`
}
```
