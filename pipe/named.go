/* -------------------------------------------------------------------------------------------
 * Copyright (c) RedShift Development LLC
 * Licensed under the MIT license. See License.txt in the project root for license information
 * ---------------------------------------------------------------------------------------- */

package pipe

import (
	"github.com/Microsoft/go-winio"

	"gitlab.com/redshift-development/rsap-go/socket"
)

type namedPipe struct {
	in     chan []byte
	listen socket.Listener
	emit   socket.Emitter
}

func NewNamedPipe(name string) (Pipe, error) {
	ln, err := winio.ListenPipe(`\\.\pipe\`+name, nil)
	if err != nil {
		return nil, err
	}
	sock := socket.NewSocket(ln)
	pipe := namedPipe{
		in:     make(chan []byte),
		listen: sock.Listen,
		emit:   sock.Emit,
	}
	go func() {
		var (
			ev socket.Event
			e  error
		)
		for {
			ev, e = pipe.listen()
			if e != nil {
				break
			}
			pipe.in <- ev.Data
		}
	}()
	return &pipe, nil
}

func (p *namedPipe) In() <-chan []byte {
	return p.in
}

func (p *namedPipe) Out(data []byte) error {
	p.emit(socket.Event{Type: "out", Data: data})
	return nil
}

func (p *namedPipe) Err(data []byte) error {
	p.emit(socket.Event{Type: "out", Data: data})
	return nil
}

type namedPipeClient struct {
	out    chan []byte
	err    chan []byte
	listen socket.Listener
	emit   socket.ClientEmitter
}

func DialNamedPipe(name string) (ChildPipe, error) {
	conn, err := winio.DialPipe(`\\.\pipe\`+name, nil)
	if err != nil {
		return nil, err
	}
	client := socket.NewClient(conn)
	pipe := namedPipeClient{
		out:    make(chan []byte),
		err:    make(chan []byte),
		listen: client.Listen,
		emit:   client.Emit,
	}
	go func() {
		var (
			ev socket.Event
			e  error
		)
		for {
			ev, e = pipe.listen()
			if e != nil {
				break
			}
			switch ev.Type {
			case "out":
				pipe.out <- ev.Data
			case "err":
				pipe.err <- ev.Data
			}
		}
	}()
	return &pipe, nil
}

func (p *namedPipeClient) In(data []byte) error {
	return p.emit(socket.Event{Data: data})
}

func (p *namedPipeClient) Out() <-chan []byte {
	return p.out
}

func (p *namedPipeClient) Err() <-chan []byte {
	return p.err
}
