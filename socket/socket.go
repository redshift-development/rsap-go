/* -------------------------------------------------------------------------------------------
 * Copyright (c) RedShift Development LLC
 * Licensed under the MIT license. See License.txt in the project root for license information
 * ---------------------------------------------------------------------------------------- */

package socket

import (
	"errors"
	"net"
	"sync"
)

type Listener func() (Event, error)
type Emitter func(event Event)
type ClientEmitter func(event Event) error

type Event struct {
	Client *client
	Type   string
	Data   []byte
}

type socket struct {
	clients    map[*client]bool
	events     chan Event
	register   chan net.Conn
	unregister chan *client
	clientMux  sync.Mutex
}

func NewSocket(ln net.Listener) *socket {
	s := socket{
		clients:    make(map[*client]bool),
		events:     make(chan Event),
		register:   make(chan net.Conn),
		unregister: make(chan *client),
	}
	go s.run()
	go func() {
		defer ln.Close()
		for {
			conn, err := ln.Accept()
			if err != nil {
				continue
			}
			s.register <- conn
		}
	}()
	return &s
}

func (s *socket) Listen() (Event, error) {
	e, ok := <-s.events
	if !ok {
		return e, errors.New("socket closed")
	}
	return e, nil
}

func (s *socket) Emit(event Event) {
	s.clientMux.Lock()
	for c := range s.clients {
		if event.Client == nil || event.Client == c {
			if c.Emit(event) != nil {
				s.unregister <- c
			}
		}
	}
	s.clientMux.Unlock()
}

func (s *socket) run() {
	for {
		select {
		case conn := <-s.register:
			s.newClient(conn)
		case c := <-s.unregister:
			s.clientMux.Lock()
			if _, ok := s.clients[c]; ok {
				delete(s.clients, c)
			}
			s.clientMux.Unlock()
		}
	}
}

func (s *socket) newClient(conn net.Conn) {
	c := NewClient(conn)
	s.clientMux.Lock()
	s.clients[c] = true
	s.clientMux.Unlock()
	go func() {
		for {
			e, err := c.Listen()
			if err != nil {
				break
			}
			s.events <- e
		}
		s.unregister <- c
	}()
}
