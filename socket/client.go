/* -------------------------------------------------------------------------------------------
 * Copyright (c) RedShift Development LLC
 * Licensed under the MIT license. See License.txt in the project root for license information
 * ---------------------------------------------------------------------------------------- */

package socket

import (
	"bufio"
	"bytes"
	"errors"
	"net"
	"sync"
)

type client struct {
	conn   net.Conn
	events chan Event
	mux    sync.Mutex
}

func NewClient(conn net.Conn) *client {
	c := &client{
		conn:   conn,
		events: make(chan Event),
	}
	go c.run()
	return c
}

func (c *client) Listen() (Event, error) {
	e, ok := <-c.events
	if !ok {
		return e, errors.New("connection closed")
	}
	return e, nil
}

func (c *client) Emit(event Event) error {
	var (
		buf  []byte
		data = bytes.NewBuffer(buf)
		err  error
	)
	data.WriteByte(byte(len(event.Type)))
	data.Write([]byte(event.Type))
	data.Write(event.Data)
	data.WriteByte('\n')
	c.mux.Lock()
	_, err = c.conn.Write(data.Bytes())
	c.mux.Unlock()
	if err != nil {
		c.conn.Close()
		return err
	}
	data.Reset()
	return nil
}

func (c *client) run() {
	r := bufio.NewReader(c.conn)
	var (
		e    Event
		err  error
		data []byte
		meta uint8
	)
	for {
		data, err = r.ReadBytes('\n')
		if err != nil {
			break
		}
		meta = data[0]
		data = data[1 : len(data)-1]
		e = Event{
			Client: c,
			Type:   string(data[:meta]),
			Data:   data[meta:],
		}
		c.events <- e
	}
	c.conn.Close()
	close(c.events)
}
