/* -------------------------------------------------------------------------------------------
 * Copyright (c) RedShift Development LLC
 * Licensed under the MIT license. See License.txt in the project root for license information
 * ---------------------------------------------------------------------------------------- */

package rsmp

import (
	"context"
	"encoding/json"

	"gitlab.com/redshift-development/rsap-go/pipe"
)

type ErrorCode int

const (
	// 100s = data errors
	ErrorBadData    ErrorCode = 100
	ErrorValidation ErrorCode = 101
	// 200s = server errors
)

type Message struct {
	RSMP   string          `json:"rsmp"`
	Action string          `json:"action"`
	Data   json.RawMessage `json:"data"`
	Error  *ErrorMessage   `json:"error,omitempty"`
	ID     string          `json:"id"`
}

type ErrorMessage struct {
	Code    ErrorCode   `json:"code"`
	Message string      `json:"message"`
	Data    interface{} `json:"data"`
}

type ActionHandler func(ctx context.Context, data interface{}) error

type service struct {
	router *router
	pipe   pipe.Pipe
}

func NewService(r *router, stdio pipe.Pipe) *service {
	srv := &service{
		router: r,
		pipe:   stdio,
	}
	return srv
}

func (s *service) Listen() {
	// TODO: add ctx and close()
	go s.in()
	go s.out()
	go s.err()
}

func (s *service) in() {
	for data := range s.pipe.In() {
		msg := Message{}
		err := json.Unmarshal(data, &msg)
		if err != nil {
			s.pipe.Err([]byte(err.Error()))
			continue
		}
		s.router.Dispatch(msg)
	}
}

func (s *service) out() {
	for msg := range s.router.out {
		data, err := json.Marshal(msg)
		if err != nil {
			s.pipe.Err([]byte(err.Error()))
			continue
		}
		s.pipe.Out(data)
	}
}

func (s *service) err() {
	var err error
	for err = range s.router.err {
		s.pipe.Err([]byte(err.Error()))
	}
}
