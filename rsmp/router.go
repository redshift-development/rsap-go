/* -------------------------------------------------------------------------------------------
 * Copyright (c) RedShift Development LLC
 * Licensed under the MIT license. See License.txt in the project root for license information
 * ---------------------------------------------------------------------------------------- */

package rsmp

// resources for later
// https://blog.golang.org/context
// https://blog.golang.org/context/gorilla/gorilla.go
// https://golang.org/pkg/context/#Context

import (
	"context"
	"encoding/json"
	"time"
)

const defaultTimeout = 30 * time.Second

type router struct {
	out        chan Message
	err        chan error
	background context.Context
	global     []func(context.Context, json.RawMessage) error
	routes     map[string][]func(context.Context, json.RawMessage) error
}

func NewRouter() *router {
	r := &router{
		out:        make(chan Message, 1),
		err:        make(chan error, 1),
		background: context.Background(),
		global:     make([]func(context.Context, json.RawMessage) error, 0),
		routes:     make(map[string][]func(context.Context, json.RawMessage) error),
	}
	return r
}

func (r *router) Global(chain ...func(context.Context, json.RawMessage) error) {
	r.global = append(r.global, chain...)
}

func (r *router) Route(action string, chain ...func(context.Context, json.RawMessage) error) {
	r.routes[action] = chain
}

func (r *router) Out() <-chan Message {
	return r.out
}

func (r *router) Err() <-chan error {
	return r.err
}

func (r *router) Dispatch(msg Message) {
	if chain, ok := r.routes[msg.Action]; ok {
		go r.dispatch(msg, append(r.global, chain...))
	}
}

func (r *router) dispatch(msg Message, chain []func(context.Context, json.RawMessage) error) {
	parentCtx, cancel := context.WithTimeout(r.background, defaultTimeout)
	ctx := &ActionContext{
		Context: parentCtx,
		message: msg,
		out:     r.out,
	}
	c := make(chan error, 1)
	for _, handler := range chain {
		go func() { c <- handler(ctx, msg.Data) }()
		select {
		case <-ctx.Done():
			<-c
			if ctx.Err() != nil {
				r.err <- ctx.Err()
			}
			break
		case err := <-c:
			if err != nil {
				r.err <- err
			}
		}
	}
	cancel()
}
